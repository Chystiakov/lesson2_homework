package ua.biz.otto;

public class Main {

    public static void main(String[] args) {
        // Training

        // 1. Positive/negative integer

        int z = 8;
        if (z > 0) {
            System.out.println("positive" + "\n");
        } else if (z < 0) {
            System.out.println("negative" + "\n");
        } else if (z == 0) {
            System.out.println("zero" + "\n");
        } else {
            System.out.println("error" + "\n");
        }

        // 2. Positive/negative double

        double w = -9.85569;
        if (w > 0) {
            System.out.println("positive" + "\n");
        } else if (w < 0) {
            System.out.println("negative" + "\n");
        } else if (w == 0) {
            System.out.println("zero" + "\n");
        } else {
            System.out.println("error" + "\n");
        }

        // 3. if 5

        int v = 389;
        int u = 5;
        String str = String.valueOf(v);
        String str1 = String.valueOf(u);
        System.out.println(v);
        if (str.contains(str1)) {
            System.out.println("it's true\n");
        } else {
            System.out.println("not true\n");
        }

        // 4&5&6&7. greater or less

        int t = -38, s = -69;
        if (t > s) {
            System.out.println("max= " + t + " min= " + s + "\n");
        } else if (s > t) {
            System.out.println("max= " + s + " min= " + t + "\n");
        } else if (s == t) {
            System.out.println("equal\n");
        } else {
            System.out.println("error\n");
        }

        // 8&9&10&11. positive/negative
        double q = -3.9768, r = 6.3291;
        if (q > 0 && r > 0) {
            System.out.println(q + " positive");
            System.out.println(r + " positive" + "\n");
        } else if (q < 0 && r < 0) {
            System.out.println(q + " negative");
            System.out.println(r + " negative\n");
        } else if (q > 0 && r < 0) {
            System.out.println(q + " positive");
            System.out.println(r + " negative\n");
        } else if (q < 0 && r > 0) {
            System.out.println(q + " negative");
            System.out.println(r + " positive\n");
        } else {
            System.out.println("error" + "\n");
        }

        if (q != 0) {
            System.out.println(q);
        }
        if (r != 0) {
            System.out.println(r);
        }
        System.out.println("\n");

        // Training Strings

        // 1&2&3&4&5 april length + second symbol
        String april = "april";
        System.out.println("april length = " + april.length());
        System.out.println("second symbol is " + april.charAt(1));
        System.out.println("last symbol is " + april.charAt(april.length() - 1));
        System.out.println("UPPER CASE = " + april.toUpperCase());
        if (april.contains("ap")) {
            System.out.println("april contains ap\n");
        }

        // 6. String tree
        String tree = "tree";
        System.out.println(tree.replace("t", "ag") + "\n");


        // Homework

        // 1. 2 largest

        int a = 3;
        int b = 6;
        int c = 9;

        if (a > b && a > c) {

            if (b > c) {
                System.out.println(a * a + b * b);
            } else {
                System.out.println(a * a + c * c);
            }

        } else if (b > a && b > c) {

            if (a > c) {
                System.out.println(b * b + a * a);
            } else {
                System.out.println(b * b + c * c);
            }

        } else if (b > a) {
            System.out.println(c * c + b * b);
        } else {
            System.out.println(c * c + a * a);
        }
        System.out.println("\n");

        // 2. Equations

        double d = 30;
        double e = 30;
        double f = 60;
        double x = 0;
        double x1 = 0;
        double x2 = 0;
        double di = e * e - 4 * d * f;
        double diSquare = Math.sqrt(di);

        if (di < 0) {
            System.out.println("No roots");
        } else if (di == 0) {
            x = -e / (2 * d);
            System.out.println("x = " + x);
        } else if (di > 0) {
            ;
            x1 = (-e + diSquare) / 2 * d;
            x2 = (-e - diSquare) / 2 * d;
            System.out.println(x1);
            System.out.println(x2);
        }
        System.out.println("\n");


        // 3.Sentences and words length

        String string =
                "Дан текст. Нужно подсчитать количество слов и предложений в тексте. Результат нужно вывести в консоль.";

        // Начальное количество слов равно 0
        int countWords = 0;
        int countSentences = 0;

        // Если ввели хотя бы одно слово, тогда считать, иначе конец программы
        if (string.length() != 0) {
            countWords++;

            // Проверяем каждый символ, пробел или точка
            for (int y = 0; y < string.length(); y++) {
                if (string.charAt(y) == ' ') {
                    // Если пробел - увеличиваем количество слов на 1
                    countWords++;
                }
                if (string.charAt(y) == '.') {
                    // Если точка - увеличиваем количество предложений на 1
                    countSentences++;
                }
            }
        }

        System.out.println("Вы ввели " + countSentences + " предложений и " + countWords + " слов");

        System.out.println("\n");


        // Palindrome
        // Работает только для английского языка. Как сделать и для русского языка?

        String text = "Was it a car or a cat I saw?";
        if (text.replaceAll("\\W", "")
                .equalsIgnoreCase(new StringBuilder(text.replaceAll("\\W", ""))
                        .reverse().toString())) {
            System.out.println("Palindrome");
        } else {
            System.out.println("Not a Palindrome");
        }

    }
}


